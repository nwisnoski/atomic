death <- function(community){

  ifelse(community$resource_assimilated < community$resource_maintenance_min,
         community$alive <- FALSE,
         community$alive <- TRUE)

  ifelse(runif(n = nrow(community)) < community$death_rate,
         community$alive <- FALSE,
         community$alive <- TRUE)

  community <- subset(community, alive == TRUE)
  return(community)
}
