init_resources <- function(x_dim, y_dim, resource_level = resource_init){
  resources <- matrix(resource_level, nrow = y_dim, ncol = x_dim)
  return(resources)
}
